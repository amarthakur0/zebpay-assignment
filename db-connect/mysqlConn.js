// Connect Mysql
'use strict';

// Required modules
let node_env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
let config = require('../config.json')[node_env];
let mysql = require('mysql');

// mysql connection
let mysqlConnection = null;

let mysqlConnFn = function (req, res, next) {
    // Check if connection
    if(!mysqlConnection) {
        mysqlConnection = mysql.createPool({
            host            : config['mysql_db_host'],
            user            : config['mysql_db_user'],
            password        : config['mysql_db_pass'],
            database        : config['mysql_db_name'],
            connectionLimit : 10
        });

        // get mysql connection
        mysqlConnection.getConnection(function(err, connection) {
            if (err) {
                console.error('Mysql connection error : ' + err.stack);                
                throw err;
            }

            console.log('Mysql connected as id : ' + connection.threadId);

            // release connection
            connection.release();

            // set mysql connection
            req.mysqlConnection = mysqlConnection;
            next();
        });
    }
    else {
        // set mysql connection
        req.mysqlConnection = mysqlConnection;
        next();
    }
}

module.exports = mysqlConnFn;