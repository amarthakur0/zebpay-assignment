'use strict';

// Required modules
let express = require('express');
let router = express.Router();

// routes
router.use('/', require('./battlesController'));

module.exports = router;