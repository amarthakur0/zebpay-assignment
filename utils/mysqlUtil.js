// Functions to Query MySQL DB
'use strict';
let Promise = require('bluebird');

let mysqlFnUtil = {
    query: function (mysqlConnection, query) {
        return new Promise(function (resolve, reject) {
            mysqlConnection.query(query, function (queryError, results, fields) {
                // error in query
                if (queryError) {
                    console.error('Mysql Query error : ' + queryError.stack);
                    return reject(queryError);
                }

                return resolve(results);
            });
        });
    },
    poolQuery: function (mysqlConnection, query) {        
        return new Promise(function (resolve, reject) {
            // get mysql connection
            mysqlConnection.getConnection(function(connErr, connection) {
                if (connErr) {
                    console.error('Mysql Pool connection error : ' + connErr.stack);
                    return reject(connErr);
                }

                console.log('Mysql Pool connected as id : ' + connection.threadId);

                // Query db
                console.log('Mysql Pool Query --> ', query);
                connection.query(query, function (queryError, results, fields) {
                    // always put connection back in pool after last query
                    connection.release();

                    // error in query
                    if (queryError) {
                        console.error('Mysql Query error : ' + queryError.stack);
                        return reject(queryError);
                    }

                    return resolve(results);
                });
            });
        });
    }
};

module.exports = mysqlFnUtil;