'use strict';

// Required modules
let express = require('express');
let path = require('path');
let bodyParser = require('body-parser');
let compression = require('compression');
let favicon = require('serve-favicon');
let app = express();
let node_env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
let config = require('./config.json')[node_env];

/*
// Connect to MySQL
let mysql = require('mysql');
let mysqlConnection = mysql.createConnection({
    host     : config['mysql_db_host'],
    user     : config['mysql_db_user'],
    password : config['mysql_db_pass']
});
mysqlConnection.connect();
console.log(mysqlConnection);
*/

// Check mysql connection
app.use(function(req, res, next) {
    require('./db-connect/mysqlConn')(req, res, next);
});

// add compression
app.use(compression());

// serve favicon
app.use(favicon(path.join(__dirname, 'public/favicon.png')));

// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// parse & session
app.use(express.static(path.join(__dirname, 'public')));

// Remove headers
app.disable('x-powered-by');

// connect to server
let server = app.listen(process.env.port || config.app_port, function () {
    console.log("App listening at http://%s:%s", server.address().address, server.address().port);
});

// All Routes here
let index = require('./routes/index');
app.use('/', index);

module.exports = app;