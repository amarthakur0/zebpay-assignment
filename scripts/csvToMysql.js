'use strict';

// Required modules
let node_env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
let config = require('../config.json')[node_env];
let mysql = require('mysql');
let fastCSV = require("fast-csv");
let path = require('path');
let mysqlFnUtil = require('../utils/mysqlUtil');

// file path
let fullFilePath = path.join(__dirname, './battles.csv');

function exitNodeProcess() {
    console.log('Exiting Node Process');
    process.exit();
}

// Connect to MySQL
let mysqlConnection = mysql.createConnection({
    host     : config['mysql_db_host'],
    user     : config['mysql_db_user'],
    password : config['mysql_db_pass'],
    database : config['mysql_db_name']
});
mysqlConnection.connect();

/*
[ 'name',
  'year',
  'battle_number',
  'attacker_king',
  'defender_king',
  'attacker_1',
  'attacker_2',
  'attacker_3',
  'attacker_4',
  'defender_1',
  'defender_2',
  'defender_3',
  'defender_4',
  'attacker_outcome',
  'battle_type',
  'major_death',
  'major_capture',
  'attacker_size',
  'defender_size',
  'attacker_commander',
  'defender_commander',
  'summer',
  'location',
  'region',
  'note' ]
*/

// Create table
const TABLE_NAME = 'battles';
let createTableQuery = 'CREATE TABLE IF NOT EXISTS '+TABLE_NAME+' (';
createTableQuery += ' id INT(11) NOT NULL AUTO_INCREMENT,';
createTableQuery += ' name VARCHAR(100) DEFAULT NULL,';
createTableQuery += ' year INT(4),';
createTableQuery += ' battle_number INT(11),';
createTableQuery += ' attacker_king VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' defender_king VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' attacker_1 VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' attacker_2 VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' attacker_3 VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' attacker_4 VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' defender_1 VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' defender_2 VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' defender_3 VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' defender_4 VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' attacker_outcome ENUM("win", "loss") DEFAULT NULL,';
createTableQuery += ' battle_type VARCHAR(30) DEFAULT NULL,';
createTableQuery += ' major_death TINYINT(1) DEFAULT 0,';
createTableQuery += ' major_capture TINYINT(1) DEFAULT 0,';
createTableQuery += ' attacker_size INT(11) DEFAULT NULL,';
createTableQuery += ' defender_size INT(11) DEFAULT NULL,';
createTableQuery += ' attacker_commander VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' defender_commander VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' summer TINYINT(1) DEFAULT 0,';
createTableQuery += ' location VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' region VARCHAR(50) DEFAULT NULL,';
createTableQuery += ' note VARCHAR(100) DEFAULT NULL,';
createTableQuery += ' PRIMARY KEY (id) )';

// console.log('Create Table Query --> ', createTableQuery);

mysqlConnection.query(createTableQuery, function (error, results, fields) {
    // error in query
    if(error) {
        console.log('Error in creating SQL Table --> ', error);
        exitNodeProcess();
    }

    console.log('----------------------');
    console.log('TABLE CREATED');
    console.log('----------------------');

    // Read CSV file
    let insertValueArr = new Array();
    fastCSV
        .fromPath(fullFilePath)
        .on("data", function(data) {
            if(data[0] !== 'name') {
                insertValueArr.push(data);
            }
        })
        .on("end", function() {
            console.log("Reading CSV Done");

            // insert to mysql
            var mysqlInsertQuery = 'INSERT INTO '+TABLE_NAME;
            mysqlInsertQuery += ' (name,';
            mysqlInsertQuery += ' year,';
            mysqlInsertQuery += ' battle_number,';
            mysqlInsertQuery += ' attacker_king,';
            mysqlInsertQuery += ' defender_king,';
            mysqlInsertQuery += ' attacker_1,';
            mysqlInsertQuery += ' attacker_2,';
            mysqlInsertQuery += ' attacker_3,';
            mysqlInsertQuery += ' attacker_4,';
            mysqlInsertQuery += ' defender_1,';
            mysqlInsertQuery += ' defender_2,';
            mysqlInsertQuery += ' defender_3,';
            mysqlInsertQuery += ' defender_4,';
            mysqlInsertQuery += ' attacker_outcome,';
            mysqlInsertQuery += ' battle_type,';
            mysqlInsertQuery += ' major_death,';
            mysqlInsertQuery += ' major_capture,';
            mysqlInsertQuery += ' attacker_size,';
            mysqlInsertQuery += ' defender_size,';
            mysqlInsertQuery += ' attacker_commander,';
            mysqlInsertQuery += ' defender_commander,';
            mysqlInsertQuery += ' summer,';
            mysqlInsertQuery += ' location,';
            mysqlInsertQuery += ' region,';
            mysqlInsertQuery += ' note)';
            mysqlInsertQuery += ' VALUES ?';

            mysqlConnection.query(mysqlInsertQuery, [insertValueArr], function (insError) {
                // error in query
                if(insError) {
                    console.log('Error in insert --> ', insError);
                    exitNodeProcess();
                }

                console.log('----------------------');
                console.log('INSERT SUCCESSFULL');
                console.log('----------------------');

                // End mysql connection
                mysqlConnection.end(function(err) {
                    // The connection is terminated now
                    if(err) console.log('Mysql conn end err -->', err);
                    console.log('Mysql conn end');

                    // all done exit now
                    exitNodeProcess();
                });
            });
        });
});

// mysqlConnection.end();